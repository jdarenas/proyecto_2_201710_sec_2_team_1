package model.Tests.pruebas;

import model.data_structures.*;
import model.logic.DataGen;
import junit.framework.TestCase;

public class RedBlackTest extends TestCase{
	
    private RBTree<String, Integer> myTree;
    
    public void setUpEscenario1()
    {
    	myTree=new RBTree<String, Integer>();
    }
    public void testSetUpEscenario2()
    {
    	setUpEscenario1();
    	DataGen generator=new DataGen();
    	int rand=(int) (1+(Math.random()*200));
    	String[] cadenas=generator.genString(rand, rand);
    	Integer[] enteros=generator.genNum(rand);   	
    	for (int i = 0; i < rand; i++) {
			myTree.put(cadenas[i], enteros[i]);
		}
    	// prueba de insercion && tamanio
    	assertEquals(rand, myTree.size());
    	//prueba eliminacion
    	for (int i = 0; i < cadenas.length; i++) {
			myTree.delete(cadenas[i]);
		}
    	assertEquals(rand/2, myTree.size());
    }
    public void testGet()
    {
    	setUpEscenario1();
    	myTree.put("Julian", 100);
    	myTree.put("Rafico", 51);
    	myTree.put("Mariana",6);
    	assertEquals(100,(int) myTree.get("Julian"));
    }
    
}
