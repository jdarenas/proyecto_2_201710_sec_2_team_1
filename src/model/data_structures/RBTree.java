package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

import sun.net.www.content.text.plain;

//
// Referencias: Robert.s, Kevin .W (http://algs4.cs.princeton.edu/33balanced/RedBlackBST.java.html)
//

public class RBTree<K extends Comparable<K>, V> {

    public static final boolean RED   = true;
    public static final boolean BLACK = false;
    private  Node root; 
    private class Node<K extends Comparable<K>,V>
    {
    	private Node izq;
    	public Node getIzq() {
			return izq;
		}
		public void setIzq(Node izq) {
			this.izq = izq;
		}
		public Node getDer() {
			return der;
		}
		public void setDer(Node der) {
			this.der = der;
		}
		public K getK() {
			return k;
		}
		public void setK(K k) {
			this.k = k;
		}
		public boolean isColor() {
			return color;
		}
		public void setColor(boolean color) {
			this.color = color;
		}
		public V getVal() {
			return val;
		}
		public void setVal(V val) {
			this.val = val;
		}
		public int getSize() {
			return size;
		}
		public void setSize(int size) {
			this.size = size;
		}
		private Node der;
    	private K k;
    	private boolean color;
    	private V val;
    	private int size;
    	public Node(K key,V val,boolean color,int size)
		{
			this.k=key;
			this.val=val;
			this.color=color;
			this.size=size;
		}
    }    
  public RBTree()
  {
  }
  public boolean isRed(Node x) {
      if (x == null) return false;
      return x.isColor() == RED;
  }

  public int size(Node x) {
      if (x == null) return 0;
      return x.getSize();
  } 


  /**
   * Returns the number of K-V pairs in this symbol table.
   * @return the number of K-V pairs in this symbol table
   */
  public int size() {
      return size(root);
  }

 /**
   * Is this symbol table empty?
   * @return {@code true} if this symbol table is empty and {@code false} otherwise
   */
  public boolean isEmpty() {
      return root == null;
  }
  /**
   * Returns the V associated with the given K.
   * @param K the K
   * @return the V associated with the given K if the K is in the symbol table
   *     and {@code null} if the K is not in the symbol table
   * @throws IllegalArgumentException if {@code K} is {@code null}
   */
  public V get(K K) {
      if (K == null) throw new IllegalArgumentException("argument to get() is null");
      return get(root, K);
  }
  public V get(Node x, K key) {
      while (x != null) {
          int cmp = key.compareTo((K) x.getK());
          if      (cmp < 0) x = x.getIzq();
          else if (cmp > 0) x = x.getDer();
          else              return  (V) x.getVal();
      }
      return null;
  }

  /**
   * Does this symbol table contain the given K?
   * @param K the K
   * @return {@code true} if this symbol table contains {@code K} and
   *     {@code false} otherwise
   * @throws IllegalArgumentException if {@code K} is {@code null}
   */
  public boolean contains(K K) {
      return get(K) != null;
  }
  /**
   * Inserts the specified K-V pair into the symbol table, overwriting the old 
   * V with the new V if the symbol table already contains the specified K.
   * Deletes the specified K (and its associated V) from this symbol table
   * if the specified V is {@code null}.
   *
   * @param K the K
   * @param val the V
   * @throws IllegalArgumentException if {@code K} is {@code null}
   */
  public void put(K K, V val) {
      if (K == null) throw new IllegalArgumentException("first argument to put() is null");
      if (val == null) {
          delete(K);
          return;
      }

      root = put(root, K, val);
      root.setColor(BLACK);
  }

  public Node put(Node h, K key, V val) { 
      if (h == null) return new Node(key, val, RED, 1);

      if(key!=null&&h!=null){
      int cmp = key.compareTo((K) h.getK());
      if      (cmp < 0) h.setIzq(put(h.getIzq(),  key, val) );
      else if (cmp > 0) h.setDer( put(h.getDer(), key, val)); 
      else              h.setVal(val);

      if (isRed(h.getDer()) && !isRed(h.getIzq()))      h = rotateLeft(h);
      if (isRed(h.getIzq())  &&  isRed(h.getIzq().getIzq())) h = rotateRight(h);
      if (isRed(h.getIzq())  &&  isRed(h.getDer()))     flipColors(h);
      h.setSize(size(h.getIzq()) + size(h.getDer()) + 1);
      }
      return h;
  }

  /**
   * Removes the smallest K and associated V from the symbol table.
   * @throws NoSuchElementException if the symbol table is empty
   */
  public void deleteMin() {
      if (isEmpty()) throw new NoSuchElementException("BST underflow");


      if (!isRed(root.getIzq()) && !isRed(root.getDer()))
          root.setColor(RED); 

      root = deleteMin(root);
      if (!isEmpty()) root.setColor(BLACK);
 
  }


  public Node deleteMin(Node h) { 
      if (h.getIzq() == null)
          return null;

      if (!isRed(h.getIzq()) && !isRed(h.getIzq().getIzq()))
          h = moveRedLeft(h);

      h.setIzq(deleteMin(h.getIzq()));
      return balance(h);
  }


  /**
   * Removes the largest K and associated V from the symbol table.
   * @throws NoSuchElementException if the symbol table is empty
   */
  public void deleteMax() {

 
      if (!isRed(root.getIzq()) && !isRed(root.getDer()))
          root.setColor(RED);

      root = deleteMax(root);
      if (!isEmpty()) root.setColor(BLACK);

  }

  
  public Node deleteMax(Node h) { 
      if (isRed(h.getIzq()))
          h = rotateRight(h);

      if (h.getDer() == null)
          return null;

      if (!isRed(h.getDer()) && !isRed(h.getDer().getIzq()))
          h = moveRedRight(h);

      h.setDer(deleteMax(h.getDer()));

      return balance(h);
  }

  /**
   * Removes the specified K and its associated V from this symbol table     
   * (if the K is in this symbol table).    
   *
   * @param  K the K
   * @throws IllegalArgumentException if {@code K} is {@code null}
   */
  public void delete(K K) { 
      if (K == null) throw new IllegalArgumentException("argument to delete() is null");
      if (!contains(K)) return;

      if (!isRed(root.getIzq()) && !isRed(root.getDer()))
          root.setColor(RED);

      root = delete(root, K);
      if (!isEmpty()) root.setColor(BLACK);
  }

  public Node delete(Node h, K K) { 

      if (K.compareTo((K) h.getK()) < 0)  {
          if (!isRed(h.getIzq()) && !isRed(h.getIzq().getIzq()))
              h = moveRedLeft(h);
          h.setIzq( delete(h.getIzq(), K));
      }
      else {
          if (isRed(h.getIzq()))
              h = rotateRight(h);
          if (K.compareTo((K) h.getK()) == 0 && (h.getDer() == null))
              return null;
          if (!isRed(h.getDer()) && !isRed(h.getDer().getIzq()))
              h = moveRedRight(h);
          if (K.compareTo((K) h.getK()) == 0) {
              Node x = min(h.getDer());
              h.setK(x.getK()); 
              h.setVal(x.getVal());
              // h.val = get(h.getDer(), min(h.getDer()).K);
              // h.K = min(h.getDer()).K;
              h.setDer(deleteMin(h.getDer())); 
          }
          else h.setDer( delete(h.getDer(), K));
      }
      return balance(h);
  }

  public Node rotateRight(Node h) {
      Node x = h.getIzq();
      h.setIzq(x.getDer());
      x.setDer(h);
      x.setColor(x.getDer().isColor());
      x.getDer().setColor(RED);
      x.setSize(h.getSize());
      h.setSize( size(h.getIzq()) + size(h.getDer()) + 1);
      return x;
  }

  public Node rotateLeft(Node h) {
      Node x = h.getDer();
      h.setDer(x.getIzq());
      x.setIzq(h);
      x.setColor(x.getIzq().isColor());
      x.getIzq().setColor(RED);
      x.setSize(h.getSize());
      h.setSize(size(h.getIzq()) + size(h.getDer()) + 1);
      return x;
  }

  public void flipColors(Node h) {
  
      h.setColor(!(h.isColor()));
      h.getIzq().setColor(!(h.getIzq().isColor()));
      h.getDer().setColor(!(h.getDer().isColor()));
  }


  public Node moveRedLeft(Node h) {


      flipColors(h);
      if (isRed(h.getDer().getIzq())) { 
          h.setDer( rotateRight(h.getDer()));
          h = rotateLeft(h);
          flipColors(h);
      }
      return h;
  }


  public Node moveRedRight(Node h) {

      flipColors(h);
      if (isRed(h.getIzq().getIzq())) { 
          h = rotateRight(h);
          flipColors(h);
      }
      return h;
  }


  public Node balance(Node h) {


      if (isRed(h.getDer()))                      h = rotateLeft(h);
      if (isRed(h.getIzq()) && isRed(h.getIzq().getIzq())) h = rotateRight(h);
      if (isRed(h.getIzq()) && isRed(h.getDer()))     flipColors(h);

      h.setSize( size(h.getIzq()) + size(h.getDer()) + 1);
      return h;
  }
  /**
   * Returns the smallest K in the symbol table.
   * @return the smallest K in the symbol table
   * @throws NoSuchElementException if the symbol table is empty
   */
  public K min() {
      if (isEmpty()) throw new NoSuchElementException("called min() with empty symbol table");
      return  (K) min(root).getK();
  } 

  // the smallest K in subtree rooted at x; null if no such K
  public Node min(Node x) { 
      // assert x != null;
      if (x.getIzq() == null) return x; 
      else                return min(x.getIzq()); 
  } 

  /**
   * Returns the largest K in the symbol table.
   * @return the largest K in the symbol table
   * @throws NoSuchElementException if the symbol table is empty
   */
  public K max() {
      if (isEmpty()) throw new NoSuchElementException("called max() with empty symbol table");
      return (K) max(root).getK();
  } 

  // the largest K in the subtree rooted at x; null if no such K
  public Node max(Node x) { 
      // assert x != null;
      if (x.getDer() == null) return x; 
      else                 return max(x.getDer()); 
  } 
  /**
   * Returns all Ks in the symbol table as an {@code Iterable}.
   * To iterate over all of the Ks in the symbol table named {@code st},
   * use the foreach notation: {@code for (K K : st.Ks())}.
   * @return all Ks in the symbol table as an {@code Iterable}
   */
  public Iterator<K>keys() {
      return (Ks(min(), max())).iterator();
  }

  /**
   * Returns all Ks in the symbol table in the given range,
   * as an {@code Iterable}.
   *
   * @param  lo minimum endpoint
   * @param  hi maximum endpoint
   * @return all Ks in the sybol table between {@code lo} 
   *    (inclusive) and {@code hi} (inclusive) as an {@code Iterable}
   * @throws IllegalArgumentException if either {@code lo} or {@code hi}
   *    is {@code null}
   */
  public ListaEncadenada<K> Ks(K lo, K hi) {
      if (lo == null) throw new IllegalArgumentException("first argument to Ks() is null");
      if (hi == null) throw new IllegalArgumentException("second argument to Ks() is null");

     ListaEncadenada<K> lista = new ListaEncadenada<K>();
      // if (isEmpty() || lo.compareTo(hi) > 0) return queue;
      Ks(root, lista, lo, hi);
      return lista;
  } 

  // add the Ks between lo and hi in the subtree rooted at x
  // to the queue
  public void Ks(Node x, ListaEncadenada<K> lista, K lo, K hi) { 
      if (x == null) return; 
      int cmplo = lo.compareTo((K) x.getK()); 
      int cmphi = hi.compareTo((K) x.getK()); 
      if (cmplo < 0) Ks(x.getIzq(), lista, lo, hi); 
      if (cmplo <= 0 && cmphi >= 0) lista.agregarElementoPrincipio((K) x.getK()); 
      if (cmphi > 0) Ks(x.getDer(), lista, lo, hi); 
  }
  /**
   * 
   * @return the root
   */
  public Node getRoot()
  {
	  return root;
  }
  /**
   * Tree inorder list
   * @param nodo the root of the tree or sub-tree to list
   * @param lista a list to be filled with the inorder of the tree
   */
  public void inOrder(Node nodo, ListaEncadenada<V> lista)
  {
	  if(nodo==null)
	  {
		  return;
	  }
	  inOrder(nodo.getIzq(),lista);
	  lista.agregarElementoFinal((V) nodo.getVal());
	  inOrder(nodo.getIzq(),lista);
  }
}