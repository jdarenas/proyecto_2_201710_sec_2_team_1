package model.data_structures;

import java.util.Iterator;

public class ListaLlaveValorSecuencia<K,T> {

	private HashNode<K,T> head;

	private int size;

	public ListaLlaveValorSecuencia()
	{
		size=0;
		head=null;
	}
	public void insert(K llave,T elem)
	{
		if(llave!=null)
		{
		boolean exists=existeLlave(llave);
		if(head==null)
		{
			head= new HashNode<>(llave, elem);
			size++;
		}
		else if(exists&&elem==null)
		{
			eliminarNodo(llave);
		}
		else if(exists&&elem!=null)
		{
			setValue(llave, elem);
		}
		else
		{
			HashNode<K,T> nodo=new HashNode<>(llave, elem);
			nodo.cammbiarSguiente(head);
			head=nodo;
			size++;
		}
		}
	}
	public boolean existeLlave(K key)
	{
		HashNode<K,T> actual=head;
		while(actual!=null)
		{
			if(actual.getKey()==key)
			{
				return true;
			}
			actual=actual.darSiguiente();
		}
		return false;
	}
	public boolean existeObjeto(T elem)
	{
		HashNode<K,T> actual=head;
		while(actual!=null)
		{
			if(actual.getValue().equals(elem))
			{
				return true;
			}
			actual=actual.darSiguiente();
		}
		return false;
	}
	public T valueKey(K key)
	{
		HashNode<K,T> actual=head;
		while(actual!=null)
		{
			if(actual.getKey().equals(key))
			{
				return actual.getValue();
			}
			actual=actual.darSiguiente();
		}
		return null;
	}
	public void eliminarNodo(K key)
	{
		HashNode<K,T> actual=head;
		HashNode<K,T> anterior=null;
		while(actual!=null)
		{
			if(actual.getKey()==key)
			{
				anterior.cammbiarSguiente(actual.darSiguiente());
				return;
			}
			anterior=actual;
			actual=actual.darSiguiente();
		}
		size--;
	}
	public void setValue(K key,T elem)
	{
		HashNode<K,T> actual=head;
		while(actual!=null)
		{
			if(actual.getKey()==key)
			{
				actual.setValue(elem);
				return ;
			}
			actual=actual.darSiguiente();
		}
	}
	public boolean isEmpty()
	{
		if(size==0)
		{
			return true;
		}
		return false;
	}
	public int size()
	{
		return size;
	}
	//
	//
	//
	@SuppressWarnings("unchecked")
	public Iterator<K> keys()
	{
		MyIter it= new MyIter(head);
		return  it;
	}
	private class MyIter implements Iterator<K>
	{
		private HashNode<K, T> head;
		
		private HashNode<K, T> actual;
		
		public MyIter(HashNode<K, T>cab) {
			head=cab;
			actual=head;
		}
		
		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			if(actual.darSiguiente()!=null)
			{
				return true;
			}
			return false;
		}

		@Override
		public K next() {
			K value=actual.darSiguiente().getKey();
			actual=actual.darSiguiente();
			return value;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}
	}
}
