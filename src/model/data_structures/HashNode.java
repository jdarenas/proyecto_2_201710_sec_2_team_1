package model.data_structures;

public class HashNode<K,T> {
	
	private HashNode<K,T> next;
	
	private K key;
	
	private T value;
	
	public HashNode(K llave,T elem)
	{
		key=llave;
		value=elem;
	}
	public T getValue()
	{
		return value;
	}
	public K getKey()
	{
		return key;
	}
	public void setValue(T elem)
	{
		value =elem;
	}
	public HashNode<K,T> darSiguiente()
	{
		return next;
	}
	public void cammbiarSguiente(HashNode<K,T> sig)
	{
		next=sig;
	}

}