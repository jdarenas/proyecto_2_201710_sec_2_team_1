package model.data_structures;

public class Stack<T> {

	private ListaEncadenada<T> stack;
	public Stack()
	{
		stack= new ListaEncadenada<T>();
	}
	public void push(T item)
	{
		stack.agregarElementoPrincipio(item);
	}
	public T pop()
	{
		T elem=stack.darElemento(0);
		stack.eliminarPrimero();	
		return elem;
	}
	public boolean isEmpty()
	{
		if(stack.darNumeroElementos()==0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public T peek()
	{
		return stack.darElemento(0);
	}
	public int size()
	{
		return stack.darNumeroElementos();
	}
}