package model.data_structures;

public class Nodo<T>{
	
	private Nodo<T> siguiente;
	
	private T elemento;
	
	public Nodo(T elem)
	{	
		elemento=elem;
		siguiente=null;
	}
	public void cambiarSiguiente(Nodo<T> sig)
	{
		siguiente=sig;
	}
	public Nodo<T> darSiguiente()
	{
		return siguiente;
	}
	public T darElemento()
	{
		return elemento;
	}
	public void cambiarElemento(T elem)
	{
		elemento=elem; 
	}
}
