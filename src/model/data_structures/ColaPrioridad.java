package model.data_structures;

import java.util.Iterator;

public class ColaPrioridad<T extends Comparable<T>> 
{
	private ListaEncadenada<T> estr;
	
	private int maximo;
	
	private int numActual;
	
	public void crearCP(int max)
	{
		maximo=max;
		estr=new ListaEncadenada<T>();
		numActual=0;
	}
	public int darNumeroElementos()
	{
		return estr.darNumeroElementos();
	}
	public void agregar(T elemento)
	{
		if(estr.darNumeroElementos()==0)
			estr.agregarElementoPrincipio(elemento);
		else
		{
			Iterator<T> iter= estr.iterator();
			boolean insertado=false;
			int pos=0;
			while(iter.hasNext()&&!insertado)
			{
				if(iter.next().compareTo(elemento)<0)
				{
					estr.insertarPos(elemento,pos);
					insertado=true;
				}		
				else
					pos++;
			}
		}
	}
	public T max()
	{
		T ans=estr.darElemento(0);
		estr.eliminarPrimero();
		return ans;
	}
	public boolean esVacia()
	{
		return estr.darNumeroElementos()==0;
	}
	public int tama�oMax()
	{
		return maximo;
	}
}
