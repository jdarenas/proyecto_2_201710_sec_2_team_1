package model.data_structures;

import java.util.Iterator;

public class EncadenamientoSeparadoTH<K,T> {

	private int size;

	private ListaLlaveValorSecuencia<K, T>[] arreglo;
	
	private ListaEncadenada<K> keys;

	public EncadenamientoSeparadoTH(int m) {
		keys=new ListaEncadenada<>();

		arreglo= new ListaLlaveValorSecuencia[m];
		for (int i = 0; i < arreglo.length; i++) {
			arreglo[i]=new ListaLlaveValorSecuencia<K,T>();
		}
		size=0;
	}

	public void insertar(K key,T val)
	{
		boolean exists=existeLlave(key);	
		int i=hash(key);
		arreglo[i].insert(key, val);
		if(exists&&val==null)
		{
			size--;
		}
		else if (!exists)
		{
			size++;
		}
	if(key!=null)
	{
		keys.agregarElementoPrincipio(key);
	}
	}
	public boolean existeLlave(K key)
	{
		int llave=hash(key);
		return arreglo[llave].existeLlave(key);
	}
	public T getVal(K key)
	{
		int i=hash(key);
		return arreglo[i].valueKey(key);
	}
	public int hash(K llave)
	{
		return  (llave.hashCode() & 0x7fffffff)%arreglo.length;
	}
	public void rehash(int size)
	{
		Iterator<K> iter=keys.iterator();
		EncadenamientoSeparadoTH<K, T> ht=new EncadenamientoSeparadoTH<>(size);
		while(iter.hasNext())
		{
			K key=iter.next();
			T val=getVal(key);
			ht.insertar(key, val);
		}
		this.arreglo=ht.arreglo;
	}
	public Iterator<K> keys()
	{
		return keys.iterator();
	}
	public int[] longitudListas()
	{
		int []lenghts=new int[arreglo.length];
		for (int i = 0; i < arreglo.length; i++) {
			lenghts[i]=arreglo[i].size();
		}
		return lenghts;
	}
	public boolean isEmpty()
	{
		return size==0?true:false;
	}
	public int size()
	{
		return size;
	}
}