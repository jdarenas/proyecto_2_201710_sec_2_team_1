package model.data_structures;

import java.lang.reflect.Array;

public class Heap<T extends Comparable<T>> {

	private T[] myArr;

	private int numeroElementos;

	private int max;

	/**
	 * 
	 * @param c clase que contiene el Heap
	 * @param n numero maximo de elemntos del Heap
	 */
	public Heap(T[] arg,int n)
	{
		numeroElementos=0;
		max=n;
		myArr=arg;
	}
	public int size()
	{
		return numeroElementos;
	}
	public void agregar(T elem)throws Exception
	{
		if(numeroElementos+1>max)
		{
			throw new Exception("Heap overflow");
		}
		else
		{
			numeroElementos++;
			myArr[numeroElementos-1]=elem;
			swim(numeroElementos-1);
		}
	}
	public T max() throws Exception
	{
		if(isEmpty())
		{
			throw new Exception("vacia");
		}
		else
		{
			T elem=myArr[0];
			myArr[0]=myArr[numeroElementos-1];
			myArr[numeroElementos-1]=null;
			numeroElementos--;
			sink(0);
			return elem;
		}
	}
	public boolean isEmpty()
	{
		if(numeroElementos==0)
		{
			return true;
		}
		return false;
	}
	public int maxSize()
	{
		return max;
	}
	public void sink(int node)
	{
		int leftChildIndex, rightChildIndex, minIndex;
		T temp;
		leftChildIndex=node*2;
		rightChildIndex=node*2+1;
		if (rightChildIndex >= numeroElementos) {
			if (leftChildIndex >= numeroElementos)
				return;
			else
				minIndex = leftChildIndex;
		}
		else
		{
			if(myArr[leftChildIndex].compareTo(myArr[rightChildIndex])<0)
			{
				minIndex=leftChildIndex;
			}
			else
			{
				minIndex=rightChildIndex;
			}
		}
		if (myArr[node].compareTo(myArr[minIndex])<0) 
		{
			temp=myArr[minIndex];
			myArr[minIndex]=myArr[node];
			myArr[node]=temp;
			sink(minIndex);
		}
	}
	public void swim(int node)
	{
		int parentindx;
		T temp;
		if(node!=0)
		{
			parentindx= (int)node/2;
			if(myArr[parentindx].compareTo(myArr[node])<0)
			{
				temp=myArr[parentindx];
				myArr[parentindx]=myArr[node];
				myArr[node]=temp;
				swim(parentindx);
			}
		}
	}
}