package model.logic;

import model.data_structures.ListaEncadenada;
import model.data_structures.Nodo;

public class QuickSort <T extends Comparable<T>> {
	
	public QuickSort(ListaEncadenada<T> lista,int desde,int hasta)
	{
		if(lista.darNumeroElementos()<=1)
		{
			int pivote=desde;
			int left=desde+1;
			int rigth=hasta;
			while(left<rigth)
			{
				while(lista.darElemento(pivote).compareTo(lista.darElemento(left))>=0)
				{
					left++;
				}
				while(lista.darElemento(pivote).compareTo(lista.darElemento(rigth))>=0)
				{
					rigth--;
				}
				if(left<rigth)
				{	
				lista.swap(left,rigth);
				}
			}
			lista.swap(pivote,left-1);
			QuickSort<T> quick1=new QuickSort<>(lista, desde, pivote-1);
			QuickSort<T> quick2=new QuickSort<>(lista, pivote+1, hasta);
		}
	}
}
