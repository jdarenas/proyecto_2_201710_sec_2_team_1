package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import jdk.nashorn.internal.runtime.arrays.IteratorAction;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import model.data_structures.*;
import model.data_structures.ListaEncadenada.Iter;
import api.ISistemaRecomendacionPeliculas;
import model.vo.*;
import sun.java2d.pipe.SpanShapeRenderer.Simple;
import model.data_structures.*;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas{


	public final static String DIR="./data/links_json.json";
	
	public final static int NEUTRAL=0;
	public final static int CONFORME=1;
	public final static int INCONFORME=2;
	public final static int NO_CLASIFICADO=3;
	
	private EncadenamientoSeparadoTH<Integer, VOPelicula> pelis;

	private ListaEncadenada<VOGeneroPelicula> ogeneros;

	private ListaEncadenada<VORating> rateadas;

	private ListaEncadenada<VOTag> tageadas;

	private ListaEncadenada<VOPelicula> anios;

	private ListaEncadenada<String> nomGeneros;

//	private ListaEncadenada<VOGeneroUsuario> usuariosNumRatingsGenero;
	
	private EncadenamientoSeparadoTH<String, VOGeneroPelicula> pelisPorGenero;

	private EncadenamientoSeparadoTH<Integer, VOUsuario> usuarios;

	private EncadenamientoSeparadoTH<String, VOUsuarioPelicula> usuariosRatingsTags;
	
	private ColaPrioridad<VOPeticion> peticionesEnCola;
	
	private  EncadenamientoSeparadoTH<Integer, ListaEncadenada<VOUsuario>> requerimiento6;
	
	private EncadenamientoSeparadoTH<String, Integer>diccionario;
	
	private RBTree<Date, EncadenamientoSeparadoTH<String, RBTree<Double, ListaEncadenada<VOPelicula>>>> requerimiento7;
	
	
//	public boolean cargarPeliculasSR(String rutaPeliculas) {
//		boolean aha=false;
//		try (BufferedReader in=new BufferedReader(new FileReader(new File(rutaPeliculas)))){
//			String linea=in.readLine();
//			while(linea!=null)
//			{
//				String args[]=linea.split(",");
//				String[] generos;
//				int anio=0;
//				String titulo="";
//				generos = args[args.length -1].split("\\|");
//				ListaEncadenada<String> gen = new ListaEncadenada<String>();
//				for (int i = 0; i < generos.length; i++)
//				{
//					gen.agregarElementoFinal(generos[i]);
//					if(!nomGeneros.existeElemento(generos[i]))
//					{
//						nomGeneros.agregarElementoFinal(generos[i]);
//					}
//				}
//				if(args.length==3)
//				{
//					String dtitle=args[1].trim();
//					String[] ano=dtitle.split("\\)");
//					anio = Integer.parseInt(ano[0].substring(ano[0].length() -4, ano[0].length()));
//					String tit[]=dtitle.split(String.valueOf(anio));
//					titulo=(tit[0].substring(0,tit[0].length()-1));
//				}
//				else
//				{
//					String[] comillas=linea.split("\"");
//					String []form=comillas[1].split("\\(");
//					titulo=form[0];
//					try
//					{
//						anio= Integer.parseInt(comillas[1].substring(comillas[1].length() -6, comillas[1].length() -2));
//					}
//					catch(NumberFormatException e)
//					{
//						anio=-1;
//					}
//					generos=comillas[2].split("\\|");
//				}
//
//				VOPelicula voPel = new VOPelicula();
//				voPel.setAgnoPublicacion(anio);
//				voPel.setGenerosAsociados(gen);
//				voPel.setTitulo(titulo);
//
//				for (int i = 0; i < gen.darNumeroElementos(); i++) 
//				{
//					VOGeneroPelicula a= new VOGeneroPelicula();
//					a.setGenero(gen.darElemento(i));
//					ListaEncadenada<VOPelicula> esta= new ListaEncadenada();
//					a.setPeliculas(esta);
//					if(!ogeneros.existeElemento(a))
//					{
//						esta.agregarElementoFinal(voPel);
//						a.setPeliculas(esta);
//						ogeneros.agregarElementoFinal(a);
//					}
//					else
//						ogeneros.darElementoPosicionActual().agregarPelicula(voPel);
//				}
//
//				pelis.agregarElementoFinal(voPel);
//
//				linea = in.readLine();
//			}
//			aha=true;
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return aha;
//
//	}
	public void cargar()
	{
		 JsonParser parser= new JsonParser();
		
		try
		{			
			double inicio= System.currentTimeMillis();
			JsonArray array=(JsonArray) parser.parse(new FileReader(new File(DIR)));
			for (int i = 0; i < array.size(); i++) 
			{
				JsonObject obj=(JsonObject) array.get(i);
				int llave =obj.get("movieId").getAsInt();
				JsonObject obj2=(JsonObject) obj.get("imdbData");
				String title=obj2.get("Title").getAsString();
				String paises=obj2.get("Country").getAsString();
				
				
				ListaEncadenada<String> listaPaises = new ListaEncadenada<String>();
				String[] count= paises.split(", ");
				for (int j = 0; j < count.length; j++) {
					String nomPays = count[j];	
					
					listaPaises.agregarElementoFinal(nomPays);
				}
				
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
				String date="22 Nov 1995";
				
				
				Date cosa=sdf.parse(date);
				
				
				String dir=obj2.get("Director").getAsString();
				String gen=obj2.get("Genre").getAsString();
				String[] generos= gen.split(",");
				
				
				String vot=obj2.get("imdbVotes").getAsString();
				String [] args=vot.split(",");
				String votosLegit="";
				for (int j = 0; j < args.length; j++) {
					votosLegit+=args[j];
				}
				int votos=0;
				if(!(votosLegit.equals("N/A"))){
				 votos=Integer.parseInt(votosLegit);
				}
				
			
				String imalgo=obj2.get("imdbRating").getAsString();
				String [] args2=imalgo.split(".");
				String rat="";
				for (int j = 0; j < args2.length; j++) {
					rat+=args2[j];
				}
				double ratingimbd=0;
				if((!(rat.equals("N/A")))&&!(rat.equals("")))
				{
					ratingimbd=Double.valueOf(rat);
				}
				VOPelicula esta=new VOPelicula();
				esta.setNombre(title);
				esta.setFechaLanzamineto(cosa);
				ListaEncadenada<VOGeneroPelicula> generosAsociados=new ListaEncadenada<>();
				esta.setVotostotales(votos);
				esta.setRatingIMBD(ratingimbd);
				esta.setMovieId(llave);
				esta.setPaises(listaPaises);
				for (int j = 0; j < generos.length; j++) 
				{
					VOGeneroPelicula a= new VOGeneroPelicula();
					a.setNombre(generos[j]);
					generosAsociados.agregarElementoPrincipio(a);	
				}
				pelis.insertar(llave, esta);
				
				
				if(llave==129)
				{
					sizeMoviesSR();
				}
//				for (int j = 0; j < generos.length; j++) 
//				{ 
//					VOGeneroPelicula a= new VOGeneroPelicula();
//					a.setNombre(generos[i]);
//					a.agregar(cosa.getTime(), esta);
//					if(!pelisPorGenero.existeLlave(generos[i]))
//					{
//						pelisPorGenero.insertar(generos[i], a);
//					}
//					else
//					{
//						pelisPorGenero.insertar(generos[i], a);
//					}
//						
//				}
				
				
			}
			double fin= System.currentTimeMillis();
			System.out.println("Tiempo de carga:"+(fin-inicio));
			cargarRatingsSR("./data/ratings.csv");
			System.out.println("ratings done");
			cargarTagsSR("./data/tags.csv");
			System.out.println("tags done");
			setUpSimilitudes();
			System.out.println("simi done");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();	
		}
	}

	


	public boolean cargarRatingsSR(String rutaRatings) {
		boolean aha=false;
		try (BufferedReader in=new BufferedReader(new FileReader(new File(rutaRatings)))){
			String linea=in.readLine();
			linea=in.readLine();
			while(linea!=null)
			{
				String args[]=linea.split(",");
				int userID=Integer.parseInt(args[0]);
				Integer movieID=Integer.parseInt(args[1]);
				double rating=Double.parseDouble(args[2]);
				long timeStamp=Long.parseLong(args[3]);			

				VORating voR = new VORating();

				VOUsuarioPelicula usuPeli=new VOUsuarioPelicula();
				VOPelicula buscada=buscarPeliPorId(movieID);
				buscada.insertarRating(userID, rating);

				usuPeli.setIdUsuario(userID);
				usuPeli.setRatingUsuario(rating);
				usuPeli.setNombrepelicula(buscada.getNombre());
				usuPeli.setLlave(buscada.getNombre()+userID);

				voR.setIdPelicula(movieID);
				voR.setIdUsuario(userID);
				voR.setRating(rating);
				voR.setTime(timeStamp);

				rateadas.agregarElementoFinal(voR);

				usuariosRatingsTags.insertar(usuPeli.getLlave(), usuPeli);

				usuariosRatingsTags.insertar(userID+buscada.getNombre(), usuPeli);


				boolean encontrada=false;
//				Iterator<VOPelicula> pato=pelis.iterator();
//				while(pato.hasNext()&&!encontrada)
//				{
//					VOPelicula madreMia= pato.next();
//					if(madreMia.getIdUsuario()==movieID)
//					{
//						madreMia.setNumeroRatings(madreMia.getNumeroRatings()+1);
//						madreMia.setPromedioRatings(((madreMia.getPromedioRatings()*madreMia.getNumeroRatings())+rating)/(madreMia.getNumeroRatings()+1));
//						encontrada=true;
//					}
//				}
				linea = in.readLine();
			}
			aha=true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aha;
	}


	public ListaEncadenada<VOUsuarioPelicula>buscarUsuarioPorId(Integer id)
	{
		ListaEncadenada<VOUsuarioPelicula> lista=new ListaEncadenada<VOUsuarioPelicula>();
		Iterator<String> llaves=usuariosRatingsTags.keys();
		while(llaves.hasNext())
		{
			VOUsuarioPelicula actual=usuariosRatingsTags.getVal(llaves.next());
			if(actual.getIdUsuario()==id)
			{
				lista.agregarElementoFinal(actual);
			}
		}
		return lista;
	}

	//
	public double calcularSimilitud(VOPelicula peli1,VOPelicula peli2)
	{
//		System.out.println("yo tambien");
//		double numerador=0;
//		double denominador1=0;
//		double denominador2=0;
//		Iterator<String> llaves=usuariosRatingsTags.keys();
//		//buscarPorTitulo
//		while(llaves.hasNext())
//		{
////			System.out.println("yo tambienx2");
//			VOUsuarioPelicula actual=usuariosRatingsTags.getVal(llaves.next());
//			Integer usId=actual.getIdUsuario();
//			double num1=buscarRatingPeliculaRateadaPorUsuario(usId, peli1);
//			double num2=buscarRatingPeliculaRateadaPorUsuario(usId, peli2);
//			numerador+=(num1*num2);
//			if(num1!=0&&num2!=0)
//			{
//				denominador1+=(num1*num1);
//				denominador2+=(num2*num2);
//			}
//		}
//		double denom= (Math.sqrt(denominador1))*(Math.sqrt(denominador2));
//		if(denom!=0&& numerador!=0)
//		{
//			return numerador/denom;
//		}
//		else
//		{
//			return 0;
//		}
		
		ListaEncadenada<Double> ratesPeli1= new ListaEncadenada<>();
		ListaEncadenada<Double> ratesPeli2= new ListaEncadenada<>();
		Iterator<Integer> iter= peli1.getRatingsAsociados().keys();
		while(iter.hasNext())
		{
			int userId= iter.next();
			double ratingPeli1=peli1.getUserRating(userId);
			double ratingPeli2=peli2.getUserRating(userId);
			if(ratingPeli1>0&&ratingPeli2>0)
			{
				ratesPeli1.agregarElementoFinal(ratingPeli1);
				ratesPeli2.agregarElementoFinal(ratingPeli2);
			}
		}
		if(ratesPeli1.darNumeroElementos()>=2&&ratesPeli2.darNumeroElementos()>=2)
		{
			Iterator<Double> iter1= ratesPeli1.iterator();
			Iterator<Double> iter2= ratesPeli2.iterator();
			double numerador=0;
			double denom1=0;
			double denom2=0;
			while(iter1.hasNext())
			{
				double rating1=iter1.next();
				double rating2=iter2.next();
				numerador+=(rating1*rating2);
				denom1+=(Math.pow(rating1, 2));
				denom2+=(Math.pow(rating2, 2));
			}
			if(denom1==0||denom2==0||numerador==0)
			{
				return 0;
			}
			double denominador=(Math.sqrt(denom1))*(Math.sqrt(denom2));
			return numerador/denominador;
		}
		return 0;
	}
	public double prediccion(VOUsuario usu,VOPelicula peli)
	{
	     double numerador=0;
	     double denominador=0;
	     Iterator<String> llaves=usuariosRatingsTags.keys();
	     while(llaves.hasNext())
	     {
	    	 VOUsuarioPelicula actual=usuariosRatingsTags.getVal(llaves.next());
	    	 if(actual.getIdUsuario()==usu.getIdUsuario()&&actual.getNombrepelicula().equals(peli.getNombre()))
	    	 {
	    		VOPelicula act=buscarPeliPorNombre(actual.getNombrepelicula());
	    		numerador+=((calcularSimilitud(peli, act))*actual.getRatingUsuario());
	    		denominador+=(calcularSimilitud(peli, act));
	    	 }
	     }
	     return numerador/denominador;
	}
	
	public VOPelicula buscarPeliPorNombre(String nombrePeli)
	{
		Iterator<Integer> llaves=pelis.keys();
		while(llaves.hasNext())
		{
			VOPelicula pelicula=pelis.getVal(llaves.next());
			if(pelicula.getNombre().equals(nombrePeli))
			{
				return pelicula;
			}
		}
		return null;
	}
	
	//
	public boolean cargarTagsSR(String rutaTags) {
		// TODO Auto-generated method stub
		boolean aha=false;
		try (BufferedReader in=new BufferedReader(new FileReader(new File(rutaTags)))){
			String linea=in.readLine();
			linea =in.readLine();
			while(linea!=null)
			{
				System.out.println(linea);
				String args[]=linea.split(",");
				int userID=Integer.parseInt(args[0]);
				int movieID=Integer.parseInt(args[1]);
				String tag="";
				for (int i = 2; i < args.length-1; i++) {
					tag+=args[i];
					
				}
				long timeStamp=Integer.parseInt(args[args.length-1]);			
				boolean yaEsta=false;
				if(usuarios.getVal(userID)!=null)
				{
					yaEsta=true;
				}
				VOUsuario user=new VOUsuario();
				VOUsuarioPelicula usuPeli=new VOUsuarioPelicula();
				usuPeli.setIdUsuario(userID);

				user.setIdUsuario(userID);
				VOTag voR = new VOTag();
				voR.setMovieId(movieID);
				voR.setUserId(userID);
				voR.setContenido(tag);
				voR.setTimeStamp(timeStamp);
				user.addTag(voR);
				if(yaEsta==false)
				{
					if(user.getPrimerTimestamp()>timeStamp)
					{
						user.setPrimerTimestamp(timeStamp);
					}
					user.addRating();
					user.addNumTags(1);
					usuarios.insertar(userID, user);
				}

				
				VOPelicula peli=buscarPeliPorId(movieID);
				if(peli!=null)
				{
				peli.addTagedUser(user);
				}

//				ListaEncadenada<VOUsuarioPelicula> usuarios=buscarUsuarioPorId(userID);
//				Iterator<VOUsuarioPelicula> iterOverUser= usuarios.iterator();
//				while(iterOverUser.hasNext())
//				{
//					iterOverUser.next().addTag(voR);
//				}
//				tageadas.agregarElementoFinal(voR);

				boolean encontrada=false;
//				Iterator<VOPelicula> pato=pelis.iterator();
//				while(pato.hasNext()&&!encontrada)
//				{
//					VOPelicula madreMia= pato.next();
//					if(madreMia.getIdUsuario()==movieID)
//					{
//						madreMia.setNumeroTags(madreMia.getNumeroTags()+1);
//						madreMia.getTagsAsociados().agregarElementoFinal(tag);
//						encontrada=true;
//					}
//				}

				linea = in.readLine();
			}
			aha=true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aha;
	}

	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return pelis.size();
	}

	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return usuarios.size();
	}

	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return tageadas.darNumeroElementos();
	}

	public long agregarPelicula(String titulo, int agno, String[] generos) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		// TODO Auto-generated method stub

	}

	@Override
	public ISistemaRecomendacionPeliculas crearSR() {
		return new SistemaRecomendacionPeliculas();
	}

	@Override
	public void registrarSolicitudRecomendacion(Integer idUsuario, String ruta) 
	{
		// TODO Auto-generated method stub
		if(peticionesEnCola==null)
		{
			peticionesEnCola.crearCP(15);
		}
			VOPeticion temp= new VOPeticion();
			temp.setRuta(ruta);
			VOUsuario user= darUsuarioID(idUsuario);
			temp.setUser(user);
			int num=peticionesEnCola.darNumeroElementos();
			ColaPrioridad<VOPeticion> temporal=peticionesEnCola;
			boolean yaTiene=false;
			while(num!=0&&!yaTiene)
			{
				VOPeticion comp=temporal.max();
				if(comp.getUser().equals(user))
				{
					yaTiene=true;
				}
				num--;
			}
			if(!yaTiene)
				peticionesEnCola.agregar(temp);
	}
	public VOUsuario darUsuarioID(Integer idUsuario)
	{
		VOUsuario ret= usuarios.getVal(idUsuario);
		return ret;
	}

	@Override
	public void generarRespuestasRecomendaciones() throws IOException 
	{
		// TODO Auto-generated method stub
		int cant=peticionesEnCola.darNumeroElementos();
		int veces=0;
		JsonObject grande=new JsonObject();
		JsonObject resp=new JsonObject();
		grande.add("response",resp);
		JsonArray uno= new JsonArray();
		while(cant!=0||veces<10)
		{
			VOPeticion at=peticionesEnCola.max();
			ColaPrioridad<VOUsuarioPelicula> pred= calcular5Predicciones(at.getUser());
			JsonObject ur=new JsonObject();
			ur.addProperty("user_id", at.getUser().getIdUsuario());
			JsonArray recs= new JsonArray();
			int catpred= pred.darNumeroElementos();
			while(catpred>0)
			{
				VOUsuarioPelicula este= pred.max();
				JsonObject rec=new JsonObject();
				rec.addProperty("item_id", buscarPeliPorNombre(este.getNombrepelicula()).getMovieId());
				rec.addProperty("p_rating", este.getRatingSistema());
				recs.add(rec);
			}
			ur.add("recommendations", recs);
			cant--;
			veces++;
		}
		resp.add("response_users", uno);
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH_mm_ss");
		Date date = new Date(); 
		FileWriter arch= new FileWriter("./data/"+dateFormat.format(date)+".json");
		try {
			arch.write((grande.toString()));
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		finally {
			arch.flush();
			arch.close();
		}
	}
	public ColaPrioridad<VOUsuarioPelicula> calcular5Predicciones(VOUsuario user)
	{
		
		//TODO
		
		ColaPrioridad<VOUsuarioPelicula> result=new ColaPrioridad<>();
		result.crearCP(5);
		ListaEncadenada<VORating> rait= ratingsUsuario(user.getIdUsuario());
		ListaEncadenada<Long> ids= new ListaEncadenada<>();
		Iterator<VORating> iterator= rait.iterator();
		while (iterator.hasNext())
		{
			ids.agregarElementoPrincipio(iterator.next().getIdPelicula());			
		}
		Iterator<Integer> iter2= pelis.keys();
		while (iter2.hasNext())
		{
			VOPelicula temp= pelis.getVal(iter2.next());
			if(!ids.existeElemento(temp.getMovieId().longValue()))
			{
				double pred= prediccion(user, temp);
				VOUsuarioPelicula a=new VOUsuarioPelicula();
				a.setNombrepelicula(temp.getNombre());
				a.setRatingSistema(pred);
				int idus=(int)user.getIdUsuario();
				a.setIdUsuario(idus);
				result.agregar(a);
			}
		}
		
		
		
		
		return result;
	}
	public ListaEncadenada<VORating> ratingsUsuario(long IdUsuario)
	{
		ListaEncadenada<VORating> ret= new ListaEncadenada<>();
		Iterator<VORating> iter= rateadas.iterator();
		while(iter.hasNext())
		{
			VORating temp= iter.next();
			if(temp.getIdUsuario()==IdUsuario)
				ret.agregarElementoPrincipio(temp);
		}
		return ret;
	}
	

	@Override
	public ILista<VOPelicula> peliculasGenero(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) 
	{
		// TODO Auto-generated method stub
		VOGeneroPelicula a= pelisPorGenero.getVal(genero.getNombre());
		ListaEncadenada<VOPelicula> ret= new ListaEncadenada<>();
		RBTree<Long, VOPelicula> nov= a.getRB();
		ListaEncadenada<Long> keys= a.getRB().Ks(fechaInicial.getTime(), fechaFinal.getTime());
		Iterator<Long> it= keys.iterator();
		while(it.hasNext())
		{
			VOPelicula add= a.getRB().get(it.next());
			ret.agregarElementoFinal(add);
		}
		return ret;
	}

	@Override
	public void agregarRatingConError(int idUsuario, int idPelicula, Double rating) 
	{
		// TODO Auto-generated method stub
		VORating rt=new VORating();
		rt.setIdPelicula(idPelicula);
		rt.setIdUsuario(idUsuario);
		rt.setRating(rating);
		rateadas.agregarElementoFinal(rt);
		VOPelicula esta=pelis.getVal(idPelicula);
		VOUsuario uju=usuarios.getVal(idUsuario);
		double pre=prediccion(uju, esta);
		VOUsuarioPelicula camb=usuariosRatingsTags.getVal(idUsuario+esta.getNombre());
		double error= pre-rating;
		if(error<0)
			error=error*-1;
		camb.setErrorRating(error);
	}

	@Override
	public ILista<VOUsuarioPelicula> informacionInteraccionUsuario(int idUsuario) {
		// TODO Auto-generated method stub
		return buscarUsuarioPorId(idUsuario);
	}

	@Override
	public void clasificarUsuariosPorSegmento() 
	{
		// TODO Auto-generated method stub�
		ListaEncadenada<VOUsuario> conformes=new ListaEncadenada<VOUsuario>();
		ListaEncadenada<VOUsuario> inconformes= new ListaEncadenada<VOUsuario>();
		ListaEncadenada<VOUsuario> neutrales= new ListaEncadenada<VOUsuario>();
		ListaEncadenada<VOUsuario> noClasificados= new ListaEncadenada<VOUsuario>();
		
		Iterator<Integer> llaves= usuarios.keys();
		while(llaves.hasNext())
		{
			int id=llaves.next();
			VOUsuario usuario=usuarios.getVal(id);
			int clasif=clasificarUsuario(id);
			if(clasif==0)
			{
				neutrales.agregarElementoFinal(usuario);
			}
			else if(clasif==1)
			{
				conformes.agregarElementoFinal(usuario);
			}
			else if(clasif==2)
			{
				inconformes.agregarElementoFinal(usuario);
			}
			else
			{
				noClasificados.agregarElementoFinal(usuario);
			}
		}
		requerimiento6.insertar(0, neutrales);
		requerimiento6.insertar(1, conformes);
		requerimiento6.insertar(2, inconformes);
		requerimiento6.insertar(3, noClasificados);
		
	}
	public int clasificarUsuario(int userId)
	{
		VOUsuario user= usuarios.getVal(userId);
		ListaEncadenada<VOTag> userTags=user.getTags();
		int neutrales=0;
		int conformes=0;
		int inconformes=0;
		int noClasificados=0;
		Iterator<VOTag> iter= userTags.iterator();
		while(iter.hasNext())
		{
			String tag=iter.next().getContenido();
			int clas=diccionario.getVal(tag);
			if(clas==0)
			{
				neutrales++;
			}
			else if(clas==1)
			{
				conformes++;
			}
			else if(clas==2)
			{
				inconformes++;
			}
			else
			{
				noClasificados=0;
			}
		}
		if(neutrales>conformes&&neutrales>inconformes&&neutrales>noClasificados)
		{
			return 0;
		}
		else if(conformes>neutrales&&conformes>inconformes&&conformes>noClasificados)
		{
			return 1;
		}
		else if(inconformes>neutrales&&inconformes>conformes&&inconformes>noClasificados)
		{
			return 2;
		}
		return 3;
	}
	

	@Override
	public void ordenarPeliculasPorAnho() {
		// TODO Auto-generated method stub
		setUpReqSiete();
	}

	@Override
	public VOReporteSegmento generarReporteSegmento(String segmento) {
		// TODO Auto-generated method stub
		VOReporteSegmento ret= new VOReporteSegmento();
		if(segmento.compareToIgnoreCase("conformes")==0)
		{
			ListaEncadenada<VOUsuario> temp=requerimiento6.getVal(1);
			
			
		}
		else if(segmento.compareToIgnoreCase("neutrales")==0)
		{
			ListaEncadenada<VOUsuario> temp=requerimiento6.getVal(0);
			 
			
		}
		else if(segmento.compareToIgnoreCase("inconformes")==0)
		{
			ListaEncadenada<VOUsuario> temp=requerimiento6.getVal(2);
		}
		else
		{
			ListaEncadenada<VOUsuario> temp=requerimiento6.getVal(3);
		}
		return ret;		
	}

	@Override
	public ILista<VOPelicula> peliculasGeneroPorFechaLanzamiento(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) {
		// TODO Auto-generated method stub
		ILista<VOPelicula> ret=new ListaEncadenada<>();
		ret=peliculasGenero(genero, fechaInicial, fechaFinal);
		return ret;		
	}
	
	//

	@Override
	public ILista<VOPelicula> peliculasMayorPrioridad(int n) {
		ListaEncadenada<VOPelicula> lista=new ListaEncadenada<VOPelicula>();
		try{
		VOPelicula [] arg=new VOPelicula[pelis.size()];
		Heap<VOPelicula> heap=new Heap<VOPelicula>(arg, pelis.size());
		Iterator<Integer> peliculas=pelis.keys();
		while(peliculas.hasNext())
		{
			VOPelicula act=pelis.getVal(peliculas.next());
			if(!(act.isComparison()))
			{
				switchCompareTo();
			}
			heap.agregar(act);
		}
		int i=0;
		while(heap.size()>0&&i<n)
		{
			lista.agregarElementoFinal(heap.max());
			i++;
		}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;
	}
	//
	
	
	@Override
	public ILista<VOPelicula> consultarPeliculasFiltros(Integer anho, String pais, VOGeneroPelicula genero) {
		ListaEncadenada<VOPelicula> lista=new ListaEncadenada<VOPelicula>();
		if(pais==null&&genero==null&&anho>=1902&&anho<=2016)
		{
			Iterator<Integer> iterPelis=pelis.keys();
			while(iterPelis.hasNext())
			{
				
				VOPelicula pelicula=pelis.getVal(iterPelis.next());
				if(pelicula.isComparison()==true)
				{
					switchCompareTo();
				}
				if(pelicula.getFechaLanzamineto().getYear()==anho)
				{
					lista.agregarElementoFinal(pelicula);
				}
			}
		}
		else if(pais!=null&&genero==null&&anho==null)
		{
			Iterator<Integer> iterPelis=pelis.keys();
			while(iterPelis.hasNext())
			{
				VOPelicula pelicula=pelis.getVal(iterPelis.next());
				if(pelicula.isComparison()==true)
				{
					switchCompareTo();
				}
				Iterator<String> paises=pelicula.getPaises().iterator();
				while(paises.hasNext())
				{
					if(paises.next().equals(pais))
					{
						lista.agregarElementoFinal(pelicula);
					}
				}
			}
		}
		else if(pais==null&&genero!=null&&anho==null)
		{
			Iterator<Integer> iterPelis=pelis.keys();
			while(iterPelis.hasNext())
			{
				VOPelicula pelicula=pelis.getVal(iterPelis.next());
				Iterator<VOGeneroPelicula> generos=pelicula.getGenerosAsociados().iterator();
				if(pelicula.isComparison()==true)
				{
					switchCompareTo();
				}
				String gen=null;
				while(generos.hasNext()&&gen==null)
				{
					String act=generos.next().getNombre();
					if(act.equals(genero))
					{
						gen=act;
					}
				}
				if(gen!=null)
				{
					lista.agregarElementoFinal(pelicula);
				}
			}
		}
		else if(pais==null&&genero!=null&&anho>=1902&&anho<=2016)
		{
			Iterator<Integer> iterPelis=pelis.keys();
			while(iterPelis.hasNext())
			{
				VOPelicula pelicula=pelis.getVal(iterPelis.next());
				if(pelicula.isComparison()==true)
				{
					switchCompareTo();
				}
				String gen=null;
				Iterator<VOGeneroPelicula>generos= pelicula.getGenerosAsociados().iterator();
				while(generos.hasNext()&&gen==null)
				{
					String act=generos.next().getNombre();
					if(act.equals(genero))
					{
						gen=act;
					}
				}
				if(pelicula.getFechaLanzamineto().getYear()==anho&&gen!=null)
				{
					lista.agregarElementoFinal(pelicula);
				}
			}
		}
		else if(pais!=null&&genero!=null&&anho==null)
		{
			Iterator<Integer> iterPelis=pelis.keys();
			while(iterPelis.hasNext())
			{
				VOPelicula pelicula=pelis.getVal(iterPelis.next());
				if(pelicula.isComparison()==true)
				{
					switchCompareTo();
				}
				String gen=null;
				Iterator<VOGeneroPelicula>generos= pelicula.getGenerosAsociados().iterator();
				while(generos.hasNext()&&gen==null)
				{
					String act=generos.next().getNombre();
					if(act.equals(genero))
					{
						gen=act;
					}
				}
				String country=null;
				Iterator<String> paises=pelicula.getPaises().iterator();
				while(paises.hasNext()&&country!=null)
				{
					String act=paises.next();
					if(act.equals(pais))
					{
						country=act;
					}
				}
				if(country!=null&&gen!=null)
				{
					lista.agregarElementoFinal(pelicula);
				}
			}
		}
		else if(pais!=null&&genero!=null&&anho>=1902&&anho<=2016)
		{
			Iterator<Integer> iterPelis=pelis.keys();
			while(iterPelis.hasNext())
			{
				VOPelicula pelicula=pelis.getVal(iterPelis.next());
				if(pelicula.isComparison()==true)
				{
					switchCompareTo();
				}
				String gen=null;
				Iterator<VOGeneroPelicula>generos= pelicula.getGenerosAsociados().iterator();
				while(generos.hasNext()&&gen==null)
				{
					String act=generos.next().getNombre();
					if(act.equals(genero))
					{
						gen=act;
					}
				}
				String country=null;
				Iterator<String> paises=pelicula.getPaises().iterator();
				while(paises.hasNext()&&country!=null)
				{
					String act=paises.next();
					if(act.equals(pais))
					{
						country=act;
					}
				}
				if(country!=null&&pelicula.getFechaLanzamineto().getYear()==anho&&gen!=null)
				{
					lista.agregarElementoFinal(pelicula);
				}
			}
		}
		QuickSort<VOPelicula> sort=new QuickSort<VOPelicula>(lista, 0, lista.darNumeroElementos()-1);
		return lista;
	}
	
	public void switchCompareTo()
	{
	 Iterator<Integer> llaves=pelis.keys();
	 while(llaves.hasNext())
	 {
		 VOPelicula pelicula=pelis.getVal(llaves.next());
		 if(pelicula.isComparison()==false)
		 {
			 pelicula.setComparison(true);
		 }
		 else
		 {
			 pelicula.setComparison(false);
		 }
	 }
	}
	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		// TODO Auto-generated method stub
		return false;
	}
	public VOPelicula buscarPeliPorId(Integer id)
	{
		return pelis.getVal(id);
	}
	
	public void setUpReqSiete()
	{
		requerimiento7= new RBTree<>();
		Iterator<Integer> llaves= pelis.keys();
		while(llaves.hasNext())
		{
			VOPelicula actual=pelis.getVal(llaves.next());
			if(!(requerimiento7.contains(actual.getFechaLanzamineto())))
			{
				EncadenamientoSeparadoTH<String, RBTree<Double, ListaEncadenada<VOPelicula>>> tabla= new EncadenamientoSeparadoTH<>(50);
				if(!(tabla.existeLlave(actual.getGenerosAsociados().darElemento(0).getNombre())))
				{
					RBTree<Double, ListaEncadenada<VOPelicula>> arbol2= new RBTree<>();
					if(!(arbol2.contains(actual.getRatingIMBD())))
					{
						ListaEncadenada<VOPelicula> peliculas= new ListaEncadenada<>();
						peliculas.agregarElementoFinal(actual);
					}
					ListaEncadenada<VOPelicula> peliculas= arbol2.get(actual.getRatingIMBD());
					peliculas.agregarElementoFinal(actual);
				}
			}
			EncadenamientoSeparadoTH<String, RBTree<Double, ListaEncadenada<VOPelicula>>> tabla=requerimiento7.get(actual.getFechaLanzamineto());
			if(!(tabla.existeLlave(actual.getGenerosAsociados().darElemento(0).getNombre())))
			{
				RBTree<Double, ListaEncadenada<VOPelicula>> arbol2= new RBTree<>();
				if(!(arbol2.contains(actual.getRatingIMBD())))
				{
					ListaEncadenada<VOPelicula> peliculas= new ListaEncadenada<>();
					peliculas.agregarElementoFinal(actual);
				}
				ListaEncadenada<VOPelicula> peliculas= arbol2.get(actual.getRatingIMBD());
				peliculas.agregarElementoFinal(actual);
			}
			RBTree<Double, ListaEncadenada<VOPelicula>> arbol2= tabla.getVal(actual.getGenerosAsociados().darElemento(0).getNombre());
			if(!(arbol2.contains(actual.getRatingIMBD())))
			{
				ListaEncadenada<VOPelicula> peliculas= new ListaEncadenada<>();
				peliculas.agregarElementoFinal(actual);
			}
			ListaEncadenada<VOPelicula> peliculas= arbol2.get(actual.getRatingIMBD());
			peliculas.agregarElementoFinal(actual);
		}
		
	}
	public void setUpSimilitudes()
	{
		 Iterator<Integer> llaves=pelis.keys();
		 int i=1;
		 while(llaves.hasNext())
		 {
				System.out.println(i++);

			 VOPelicula pelicula=pelis.getVal(llaves.next());
			 Iterator<Integer> llaves2=pelis.keys();
			 while(llaves2.hasNext())
			 {
				 VOPelicula pelicula2=pelis.getVal(llaves2.next());
				 double sim=calcularSimilitud(pelicula, pelicula2);
				 pelicula.insertSim(pelicula2.getMovieId(), sim);
			 }
			 
		 }
	}
	
	
	public void cargarDiccionario(String path)
	{
		try(BufferedReader in=new BufferedReader(new FileReader(new File(path))))
		{
			String line=in.readLine();
			line=in.readLine();
			while(line!=null)
			{
				String[] args= line.split(",");
				String tag=args[0];
				String segmento=args[1];
				int value=3;
				if(segmento.equals("neutral"))
				{
					value=0;
				}
				else if(segmento.equals("conforme"))
				{
					value=1;
				}
				else if(segmento.equals("inconforme"))
				{
					value=2;
				}
				diccionario.insertar(tag,value);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public SistemaRecomendacionPeliculas()
	{
		pelis=new EncadenamientoSeparadoTH<Integer, VOPelicula>(100);
		rateadas=new ListaEncadenada<VORating>();
		usuariosRatingsTags= new EncadenamientoSeparadoTH(100);
		usuarios=new EncadenamientoSeparadoTH<Integer, VOUsuario>(100);
		tageadas= new ListaEncadenada<VOTag>();
	}
	
	
}