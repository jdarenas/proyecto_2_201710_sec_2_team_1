package model.vo;

public class VOPeticion implements Comparable<VOPeticion>
{
	private VOUsuario user;
	
	private String ruta;
	
	public VOUsuario getUser() {
		return user;
	}

	public void setUser(VOUsuario user) {
		this.user = user;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	@Override
	public int compareTo(VOPeticion o) {
		// TODO Auto-generated method stub
		if(ruta==null&&o.getRuta()==null)
		{
			if(user.getPrimerTimestamp()<o.getUser().getPrimerTimestamp())
			{
				return -1;
			}
			else if(user.getPrimerTimestamp()>o.getUser().getPrimerTimestamp())
			{
				return 1;
			}
			else
			{
				if(user.getNumRatings()>o.getUser().getNumRatings())
				{
					return 1;
				}
				else if(user.getNumRatings()==o.getUser().getNumRatings())
				{
					if(user.getNumTags()>o.getUser().getNumTags())
						return 1;
					else if(user.getNumTags()<o.getUser().getNumTags())
						return -1;
					else
						return 0;
				}
				else
					return -1;
			}
		}
		else if(ruta!=null&&o.getRuta()==null)
		{
			return -1;
		}
		else
			return 1;

	}

	
}
