package model.vo;

public class VOTag {
	
	/**
	 * contenido del tag
	 */
	private String contenido;
	/**
	 * 
	 */
	private int userId;
	/**
	 * 
	 */
	private long movieId;
	/**
	 * 
	 */
	private long timeStamp;
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public long getMovieId() {
		return movieId;
	}

	public void setMovieId(long movieId) {
		this.movieId = movieId;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public VOTag() {
		// TODO Auto-generated constructor stub
	}
	
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

}
