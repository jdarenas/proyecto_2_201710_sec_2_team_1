package model.vo;

import java.util.Date;

import model.data_structures.*;

public class VOPelicula implements Comparable<VOPelicula> {
	/*
	 * nombre de la pel�cula
	 */
	private String nombre;
	/*
	 * 
	 */
	private ListaEncadenada<String> paises;
	/*
	 * Fecha de lanzamiento de la pel�cula
	 */
	private Date fechaLanzamineto;
	
	/*
	 * Lista con los generos asociados a la pel�cula
	 */
	
	private ILista<VOGeneroPelicula> generosAsociados;
	
	/*
	 * votos totales sobre la pel�cula
	 */
	 private int votostotales;  
	 
	 /*
	  * promedio anual de votos (hasta el 2016)
	  */
	 private int promedioAnualVotos; 
	 
	 /*
	  * promedio IMBD
	  */
	 
	 private double ratingIMBD;	
	 
	 /**
	  * 
	  */
	 private Integer movieId;
		/**
	  * True:compareTo1    false:compareTo2
	  */
	 private boolean comparison;
	 
	 /**
	  * 
	  */
	 private EncadenamientoSeparadoTH<Integer,Double> similitudes;
	 
	 
	 private EncadenamientoSeparadoTH<Integer, VOUsuario> usuariosTageados;
	 
	 
	 private EncadenamientoSeparadoTH<Integer, Double> ratingsAsociados;


	public Integer getMovieId() {
		return movieId;
	}

	public EncadenamientoSeparadoTH<Integer, Double> getRatingsAsociados() {
		return ratingsAsociados;
	}

	public void setRatingsAsociados(EncadenamientoSeparadoTH<Integer, Double> ratingsAsociados) {
		this.ratingsAsociados = ratingsAsociados;
	}
	public void insertarRating(int userId,double rat)
	{
		ratingsAsociados.insertar(userId, rat);
	}
	
	public double getUserRating(int userId)
	{
		return (ratingsAsociados.getVal(userId)==null)?0:ratingsAsociados.getVal(userId);
	}

	public void setMovieId(Integer movieId) {
		this.movieId = movieId;
	}

	public EncadenamientoSeparadoTH<Integer, Double> getSimilitudes() {
		return similitudes;
	}

	public void setSimilitudes(EncadenamientoSeparadoTH<Integer, Double> similitudes) {
		this.similitudes = similitudes;
	}

	public EncadenamientoSeparadoTH<Integer, VOUsuario> getUsuariosTageados() {
		return usuariosTageados;
	}

	public void setUsuariosTageados(EncadenamientoSeparadoTH<Integer, VOUsuario> usuariosTageados) {
		this.usuariosTageados = usuariosTageados;
	}


	 
	 public boolean isComparison() {
		return comparison;
	}

	public void setComparison(boolean comparison) {
		this.comparison = comparison;
	}

	public VOPelicula() {
		// TODO Auto-generated constructor stub
		comparison=true;
		similitudes=new EncadenamientoSeparadoTH<Integer, Double>(50);
		usuariosTageados= new EncadenamientoSeparadoTH(20);
		ratingsAsociados= new EncadenamientoSeparadoTH<>(20);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaLanzamineto() {
		return fechaLanzamineto;
	}

	public void setFechaLanzamineto(Date fechaLanzamineto) {
		this.fechaLanzamineto = fechaLanzamineto;
	}

	public ILista<VOGeneroPelicula> getGenerosAsociados() {
		return generosAsociados;
	}

	public void setGenerosAsociados(ILista<VOGeneroPelicula> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}

	public int getVotostotales() {
		return votostotales;
	}

	public void setVotostotales(int votostotales) {
		this.votostotales = votostotales;
	}

	public int getPromedioAnualVotos() {
		return promedioAnualVotos;
	}

	public void setPromedioAnualVotos(int promedioAnualVotos) {
		this.promedioAnualVotos = promedioAnualVotos;
	}

	public double getRatingIMBD() {
		return ratingIMBD;
	}

	public void setRatingIMBD(double ratingIMBD) {
		this.ratingIMBD = ratingIMBD;
	}
	
	
	public void addTagedUser(VOUsuario user)
	{
		usuariosTageados.insertar(user.getIdUsuario(), user);;
	}
	
	
	public void insertSim(Integer k,Double val)
	{
		similitudes.insertar(k, val);
	}
	
	public ListaEncadenada<String> getPaises() {
		return paises;
	}

	public void setPaises(ListaEncadenada<String> paises) {
		this.paises = paises;
	}

	@Override
	public int compareTo(VOPelicula o) {
		 if(o==o)
		 {
				int years1=2016-fechaLanzamineto.getYear();
				int years2=2016-o.getFechaLanzamineto().getYear();
				if((years1*promedioAnualVotos*ratingIMBD)<(years2*o.getPromedioAnualVotos()*o.getPromedioAnualVotos()))
				{
					return -1;
				}
				else if((years1*promedioAnualVotos*ratingIMBD)==(years2*o.getPromedioAnualVotos()*o.getPromedioAnualVotos()))
				{
					return 0;
				}
				else
				{
					return 1;
				}
		 }
		 else
		 {
			 return this.compareTo2(o);
		 }
	}
	
	public int compareTo2(VOPelicula o)
	{
		int cmp1=fechaLanzamineto.compareTo(o.getFechaLanzamineto());
		if(cmp1==0)
		{
			int cmp2=paises.darElemento(0).compareTo(o.getPaises().darElemento(0));
			if(cmp2==0)
			{
				int cmp3=generosAsociados.darElemento(0).getNombre().compareTo(o.generosAsociados.darElemento(0).getNombre());
				if(cmp3==0)
				{
					
				}
				cmp1=cmp3;
			}
			cmp1=cmp2;
		}
		return cmp1;
	}
	 
	 
	
}
