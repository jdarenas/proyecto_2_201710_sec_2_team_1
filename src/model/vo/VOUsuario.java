package model.vo;

import model.data_structures.EncadenamientoSeparadoTH;
import model.data_structures.ListaEncadenada;

public class VOUsuario implements Comparable<VOUsuario>{

	public final static int CONFORME=1;
	
	public final static int INCONFORME=2;
	public final static int NEUTRAL=3;
	public final static int NOCLASIF=0;
	
	private int idUsuario;	
	private long primerTimestamp;
	private int numRatings;
	private int numTags;
	private ListaEncadenada<VOTag> tags;
	
	public void addTag(VOTag tag)
	{
		tags.agregarElementoFinal(tag);
	}
	public ListaEncadenada<VOTag> getTags() {
		return tags;
	}
	public void setTags(ListaEncadenada<VOTag> tags) {
		this.tags = tags;
	}
	public int getNumTags() {
		return numTags;
	}
	public void setNumTags(int numTags) {
		this.numTags = numTags;
	}
	public void addNumTags(int numTags) {
		this.numTags += numTags;
	}
	private double diferenciaOpinion;

	private EncadenamientoSeparadoTH<Integer, Double> predicciones;
	


	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getPrimerTimestamp() {
		return primerTimestamp;
	}
	public void setPrimerTimestamp(long primerTimestamp) {
		this.primerTimestamp = primerTimestamp;
	}
	public int getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	public void addRating()
	{
		numRatings++;
	}
	public double getDiferenciaOpinion() {
		return diferenciaOpinion;
	}
	public void setDiferenciaOpinion(double diferenciaOpinion) {
		this.diferenciaOpinion = diferenciaOpinion;
	}
	@Override
	public int compareTo(VOUsuario o) {
		if(primerTimestamp==o.getPrimerTimestamp())
		{
			if(numRatings==o.getNumRatings())
			{
				if(idUsuario==o.getIdUsuario())
				{
					return 0;
				}
				else if(idUsuario<o.getIdUsuario())
				{
					return -1;
				}
				else
				{
					return 1;
				}
			}
			else if(numRatings<o.getNumRatings())
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}
		else if(primerTimestamp<o.getPrimerTimestamp())
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
	
}
