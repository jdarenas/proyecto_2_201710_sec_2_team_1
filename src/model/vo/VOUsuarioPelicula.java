package model.vo;

import model.data_structures.*;
import model.vo.VOTag;

/**
 * @author Venegas
 *
 */

public class VOUsuarioPelicula implements Comparable<VOUsuarioPelicula> {
	
	
	
	
	/**
	 * 
	 */
	private String Llave; 
	public String getLlave() {
		return Llave;
	}

	public void setLlave(String llave) {
		Llave = llave;
	}

	/**
	 * nombre de la pel�cula
	 */

	private String nombrepelicula;
	
	/**
	 * rating dado por el usuario
	 */
	private double ratingUsuario;
	
	/**
	 * rating calculado por el sistema
	 */

	private double ratingSistema;
	
	/**
	 * error sobre el rating
	 */
	
	private double errorRating;
	
	/**
	 * list de tags generados por el usuario sobre la pelicula
	 */
	
	private ILista<VOTag> tags;
	
	/**
	 * id del usuario
	 */
	
	private Integer idUsuario;
	/**
	 * 
	 */
	
	/**
	 * 
	 */

	public VOUsuarioPelicula() {
		// TODO Auto-generated constructor stub
		tags=new ListaEncadenada<>();

		tags = new ListaEncadenada<VOTag>();
	}
	
	public String getNombrepelicula() {
		return nombrepelicula;
	}

	public void setNombrepelicula(String nombrepelicula) {
		this.nombrepelicula = nombrepelicula;
	}

	public double getRatingUsuario() {
		return ratingUsuario;
	}

	public void setRatingUsuario(double ratingUsuario) {
		this.ratingUsuario = ratingUsuario;
	}

	public double getRatingSistema() {
		return ratingSistema;
	}

	public void setRatingSistema(double ratingSistema) {
		this.ratingSistema = ratingSistema;
	}

	public double getErrorRating() {
		return errorRating;
	}

	public void setErrorRating(double errorRating) {
		this.errorRating = errorRating;
	}

	public ILista<VOTag> getTags() {
		return tags;
	}

	public void setTags(ILista<VOTag> tags) {
		this.tags = tags;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public void addTag(VOTag tag)
	{
		tags.agregarElementoFinal(tag);
	}

	@Override
	public int compareTo(VOUsuarioPelicula o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
