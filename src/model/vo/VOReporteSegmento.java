package model.vo;
import model.data_structures.*;
/**
 * Esta clase modela el reporte que se debe generar por segmento
 * @author Venegas
 *
 */
public class VOReporteSegmento{


/*
	 *  el error promedio  (suma de errores sobre sus ratings dividido la cantidad de ratings con 
	 *error asociado)
	 */
	private double errorPromedio;
	
	/*
	 * 5 g�neros con m�s cantidad de ratings
	 * 
	 */
	
	private ILista<VOGeneroPelicula> generosMasRatings;

	/*
	 *  sus 5 g�neros con mejor rating promedio.
	 */
	
	private ILista<VOGeneroPelicula> generosMejorPromedio;
	
	
	public VOReporteSegmento() {
		// TODO Auto-generated constructor stub
	}


	public double getErrorPromedio() {
		return errorPromedio;
	}


	public void setErrorPromedio(double errorPromedio) {
		this.errorPromedio = errorPromedio;
	}


	public ILista<VOGeneroPelicula> getGenerosMasRatings() {
		return generosMasRatings;
	}


	public void setGenerosMasRatings(ILista<VOGeneroPelicula> generosMasRatings) {
		this.generosMasRatings = generosMasRatings;
	}


	public ILista<VOGeneroPelicula> getGenerosMejorPromedio() {
		return generosMejorPromedio;
	}


	public void setGenerosMejorPromedio(ILista<VOGeneroPelicula> generosMejorPromedio) {
		this.generosMejorPromedio = generosMejorPromedio;
	}
}
