package model.vo;

import java.sql.Date;

import model.data_structures.RBTree;

public class VOGeneroPelicula {

	/*
	 * nombre del genero
	 */
	
	private String nombre;
	
	private RBTree<Long, VOPelicula> peliculasAsociadas;
	
	public VOGeneroPelicula() {
		// TODO Auto-generated constructor stub
		peliculasAsociadas=new RBTree<>();
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void agregar(Long o, VOPelicula a)
	{
		peliculasAsociadas.put(o, a);
	}
	public RBTree<Long, VOPelicula> getRB() {
		return peliculasAsociadas;
	}
}
