package model.vo;

public class VORating implements Comparable<VORating>{
	
	private long idUsuario;
	private long idPelicula;
	private double rating;
	private long timeStamp;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public long getTime() {
		return timeStamp;
	}
	public void setTime(long time) {
		timeStamp = time;
	}
	@Override
	public int compareTo(VORating o) {
		// TODO Auto-generated method stub
		if(rating==o.getRating())
			return 0;
		else if(rating<o.getRating())
			return -1;
		else
			return 1;
	}

}
