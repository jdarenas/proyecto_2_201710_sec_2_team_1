package view;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

import model.data_structures.EncadenamientoSeparadoTH;
import model.logic.SistemaRecomendacionPeliculas;
import model.vo.VOPelicula;

public class Main {


	static BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(System.out));
	static Scanner lector = new Scanner(System.in);
	static SistemaRecomendacionPeliculas sistema;
	public static void main(String[] args) {
		sistema=new SistemaRecomendacionPeliculas();

		ClienteReq cliente = new ClienteReq(escritor, lector);
		int opcion = -1;

		//TODO: Inicializar objetos 

		while (opcion != 0) {
			try {
				escritor.write("---------------Cliente Pruebas Proyecto 2---------------\n");
				escritor.write("Ingrese un numeral\n");
				escritor.write("Opciones:\n");
				escritor.write("1: Cargar Peliculas al Sistema de Recomendaciones. \n");
				escritor.write("2: Cargar Ratings de los usuarios al SR. \n");
				escritor.write("3: Cargar Tags al Sistema de Recomendaciones \n");
				escritor.write("4: Menú requerimientos \n");
				escritor.write("0: Salir\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();

				switch(opcion) {
				case 1: r1(); break;
				case 2: r2(); break;
				case 3: r3(); break;
				case 4: cliente.pruebas(); break;
//				case 5: r6();break;
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}

		try {
			escritor.write("Chao");
			escritor.flush();
			escritor.close();
			lector.close();
		}
		catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}

	private static void r1() throws IOException{

		//TODO: Cargue la lista de peliculas al Sistema de Recomendación.
		long tiempo = System.nanoTime();
		//TODO: Llamar método cargarPeliculasSR(String rutaPeliculas) del API
		sistema.cargar();
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		//Se espera como resultado: la confirmación de carga de archivos}
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private static void r2() throws IOException{

		//TODO: Cargue los ratings al Sistema de Recomendación.
		
		sistema.cargar();
		long tiempo = System.nanoTime();

		//TODO: Llamar método cargarRatingsSR(String rutaRatings) del API
		//Se espera como resultado: la confirmación de carga de archivos
		sistema.cargarRatingsSR("./data/ratings.csv");

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private static void r3() throws IOException{

		//TODO: Cargue los tags al Sistema de Recomendación.
	
		long tiempo = System.nanoTime();

		//TODO: Llamar método cargarTagsSR(String rutaTags) del API
		sistema.cargarTagsSR("./data/tags.csv");
		//Se espera como resultado: la confirmación de carga de archivos
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
//	private static void r6() throws IOException
//	{
//		sistema.cargar();
//		escritor.write("digite nombre peli");
//		String pel= lector.next();
//		VOPelicula peli=sistema.buscarPeliPorNombre(pel);
//		if(peli==null)
//		{
//			escritor.write("no fue encontrada");
//			escritor.flush();
//			return;
//		}
//		escritor.write(peli.getNombre()+" "+peli.getPromedioAnualVotos()+" "+peli.getFechaLanzamineto().toString() );
//		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
//		escritor.flush();
//		lector.next();
//	}

}