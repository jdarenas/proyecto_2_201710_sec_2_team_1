package view;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;


public class ClienteReq {

	BufferedWriter escritor;
	Scanner lector;


	//TODO: Declarar objetos

	public ClienteReq(BufferedWriter escritor, Scanner lector) {
		this.escritor = escritor;
		this.lector = lector;
	}

	public void pruebas() {
		int opcion = -1;

		//TODO: Inicializar objetos 


		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto ---------------\n");
				escritor.write("Requerimientos:\n");
				escritor.write("1: Registrar solicitud de recomendaciÃ³n a un usuario. (R1) \n");
				escritor.write("12: Registrar solicitud de recomendaciÃ³n mediante archivo .JSON (R1) \n");
				escritor.write("2: Generar archivo de respuesta a solicitud de usuarios. (R2) \n");
				escritor.write("3: Generar listado ordenado por fecha de peliculas en un gÃ©nero especÃ­fico dentro de un rango de fechas. (R3) \n");
				escritor.write("4: AnÌƒadir un nuevo rating a una peliÌ�cula y guardar el error de prediccioÌ�n (R4) \n");
				escritor.write("5: Generar un informe con toda la informacioÌ�n que se tiene sobre un usuario y su interaccioÌ�n con las peliÌ�culas. (R5) \n");
				escritor.write("6: Se desea tener una lista con los identificadores de usuarios clasificados en un segmento dado.(R6) \n");
				escritor.write("7: Ordenar las peliÌ�culas del cataÌ�logo en un aÌ�rbol binario balanceado y ordenarlo por el anÌƒo de la peliÌ�cula. (R7) \n");
				escritor.write("8: Realizar un reporte para describir los segmentos de los usuarios.(R8) \n");
				escritor.write("9: Buscar peliÌ�culas de un geÌ�nero cuya fecha de lanzamiento se encuentre en un periodo de tiempo dado por diÌ�a /mes /anÌƒo inicial y diÌ�a /mes /anÌƒo final (R9) \n");
				escritor.write("10: Obtener las N peliÌ�culas de mayor prioridad en su orden. (R10) \n");
				escritor.write("11: Generar lista de peliculas de acuerdo a los 3 criterios ingresados por el usuario (R11) \n");
				escritor.write("0: Volver\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();

				switch(opcion) {
				case 1: r1(); break;
				case 2: r2(); break;
				case 3: r3(); break;
				case 4: r4(); break;
				case 5: r5(); break;
				case 6: r6(); break;
				case 7: r7(); break;
				case 8: r8(); break;
				case 9: r9(); break;
				case 10: r10(); break;
				case 11: r11(); break;
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			catch (NumberFormatException nfe) {
				try {
					escritor.write("No ingreso el periodo de tiempo correctamente\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}

	private void r1() throws IOException{

		//TODO: Registrar solicitud de recomendaciÃ³n a un usuario. Recuerde que hay dos tipos de solicitud
		//RECUERDE: Usted debe manejar si el parametro es por usuario con ID o la ruta del JSON.
		
		

		escritor.write("Ingrese el ID del usuario: \n");
		escritor.flush();
		String usuarioID = lector.next();
		System.out.println(usuarioID);
		// opciÃ³n por JSON:
		//escritor.write("Ingrese la ruta JSON: \n");
		//escritor.flush();
		//String rutaJson = lector.next();
		//System.out.println(rutaJson);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo registrarSolicitudRecomendacion(Integer idUsuario, String ruta) del API

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}


	private void r2() throws IOException{

		//TODO: Se debe generar un archivo con la respuesta a las 10 solicitudes de recomendacioÌ�n de mayor prioridad en un archivo json con el formato indicado.
		//RECUERDE: El nombre del archivo a generar debe tener el siguiente formato Recomendaciones_<Dia-Mes-AnÌƒo_Hora_min_seg>.json


		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo generarRespuestasRecomendaciones() del API
		//Se espera como resultado: ruta del archivo generado. 

		String ruta = "";
		tiempo = System.nanoTime() - tiempo;
		escritor.write("El archivo se creÃ³ con la ruta: " + ruta + "\n");
		escritor.write("\n");

		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r3() throws NumberFormatException, IOException
	{
		//TODO: retornar un listado ordenado por fecha de todas las peliÌ�culas de un geÌ�nero particular dentro de un rango de fechas dado
		//RECUERDE: diÌ�a /mes /anÌƒo inicial â€“ diÌ�a /mes/ anÌƒo final, usando el mismo formato de fecha que aparece en los datos de las peliÌ�culas

		
		escritor.write("Ingrese la fecha inicial: \n");
		escritor.flush();
		String fechaInicial = lector.next();
		System.out.println(fechaInicial);

		escritor.write("Ingrese la fecha final: \n");
		escritor.flush();
		String fechaFinal = lector.next();
		System.out.println(fechaFinal);

		escritor.write("Ingrese el genero a buscar: \n");
		escritor.flush();
		String genero = lector.next();
		System.out.println(genero);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato de fechas y genero que usted maneja en su proyecto.
		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo peliculasGeneroPorFechas(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) del API
		//Se espera como resultado: lista VOPeliculas
  
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r4() throws IOException
	{
		//TODO: Se desea que los usuarios puedan seguir anÌƒadiendo ratings a las peliÌ�culas; cada vez que un usuario anÌƒade un rating se compara el rating dado con la prediccioÌ�n dada por el sistema.
		//RECUERDE: El valor absoluto de la diferencia entre el rating otorgado por el usuario y la prediccioÌ�n del sistema es el error.
		//RECUERDE: Cada vez que se anÌƒade un rating se debe tener registro de cuaÌ�nto fue el error para el rating otorgado.

	
		escritor.write("Ingrese el ID del Usuario: \n");
		escritor.flush();
		String idUsuario = lector.next();
		System.out.println(idUsuario);

		escritor.write("Ingrese el ID de la pelicula: \n");
		escritor.flush();
		String idPelicula = lector.next();
		System.out.println(idPelicula);

		escritor.write("Ingrese el nuevo Rating \n");
		escritor.flush();
		String rating = lector.next();
		System.out.println(rating);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato que usted maneja en su proyecto.
		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo agregarRatingConError(int idUsuario, int idPelicula, Double rating) del API

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r5() throws IOException{

		//TODO: generar un informe con toda la informacioÌ�n que se tiene sobre un usuario y su interaccioÌ�n con las peliÌ�culas.
		// RECUERDE: informacion a retornar en la lista: nombre Pelicula, rating otorgado por el usuario, rating calculado por el sistema,
		//error del rating, tags que el usuario otorgÃ³ a la pelÃ­cula. 
		
		//RECUERDE: El error se calcula como el valor absoluto de la diferencia entre los ratings. 
		escritor.write("Ingrese el ID del usuario: \n");
		escritor.flush();
		String usuarioID = lector.next();
		System.out.println(usuarioID);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo informacionInteraccionUsuario(int idUsuario) del API
		//Se espera como resultado: lista VOUsuarioPelicula 

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r6() throws IOException{

		//TODO:Se desea tener una lista con los identificadores de usuarios clasificados en un segmento dado.
		//RECUERDE: 4 tipos o segmentos diferentes: Inconformes, Conformes, Neutrales y No Clasificado.


		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo clasificarUsuariosPorSegmento() del API
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r7() throws IOException{

		//TODO:ordenar las peliÌ�culas del cataÌ�logo en un aÌ�rbol binario balanceado y ordenado por el anÌƒo de la peliÌ�cula.

		//RECUERDE:Dentro de cada nodo del aÌ�rbol, se tiene una lista, en la que en cada nodo se tiene una peliÌ�cula y 
		//una tabla de hash con los usuarios que han asignado un tag especiÌ�fico a dicha peliÌ�cula.



		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo ordernarPelicuaPorAnho() del API

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r8() throws IOException{

		//TODO:  Dado un segmento se quiere saber lo siguiente: el error promedio, sus 5 geÌ�neros con maÌ�s cantidad de ratings y sus 5 geÌ�neros con mejor rating promedio.
		//RECUERDE: Error promedio = suma de errores sobre sus ratings dividido la cantidad de ratings con error asociado. 

		escritor.write("Ingrese el segmento: \n");
		escritor.flush();
		String segmento = lector.next();
		System.out.println(segmento);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo generarReporteSegmento(String segmento) del API 
		//Se espera como resultado: VOReporteSegmento 
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r9() throws NumberFormatException, IOException
	{
		//TODO: Buscar peliÌ�culas de un geÌ�nero cuya fecha de lanzamiento se encuentre en un periodo de tiempo dado
		//RECUERDE: diÌ�a /mes /anÌƒo inicial â€“ diÌ�a /mes/ anÌƒo final, usando el mismo formato de fecha que aparece en los datos de las peliÌ�culas
		//RECUERDE: Tenga en cuenta que para las peliÌ�culas, su mes de lanzamiento (campo Released) estaÌ� dado por las 3 iniciales en ingleÌ�s (Jan, Feb, Mar, Apr, ...). 

		escritor.write("Ingrese la fecha inicial: \n");
		escritor.flush();
		String fechaInicial = lector.next();
		System.out.println(fechaInicial);

		escritor.write("Ingrese la fecha final: \n");
		escritor.flush();
		String fechaFinal = lector.next();
		System.out.println(fechaFinal);

		escritor.write("Ingrese el genero a buscar: \n");
		escritor.flush();
		String genero = lector.next();
		System.out.println(genero);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato de fechas y genero que usted maneja en su proyecto.
		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo peliculasGeneroPorFechaLanzamiento(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) del API 
		//Se espera como resultado: lista VOPelicula		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}


	private void r10() throws IOException{

		//TODO: A partir del Heap se quiere obtener las N peliÌ�culas de mayor prioridad en su orden, donde el valor N es dado por el usuario.
		//RECUERDE:  Para cada peliÌ�cula resultante, hay que mostrar su tiÌ�tulo, anÌƒo, votos totales, promedio anual votos y prioridad de clasificacioÌ�n. 

		escritor.write("Ingrese el N a buscar en el SR.\n");
		escritor.flush();
		int peliculas = lector.nextInt();
		System.out.println(peliculas);

		//RECUERDE: Utilice la variable previamente declarada y adaptela al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo peliculasMayorPrioridad(int n) del API 
		//Se espera como resultado: lista VOPelicula		
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r11() throws IOException{

		//TODO: Las peliÌ�culas resultantes deben obtenerse en el siguiente orden: Inicialmente ordenadas por anÌƒo. 
		//Para las peliÌ�culas del mismo anÌƒo, deben ordenarse por paiÌ�s (alfabeÌ�ticamente). 
		//Para peliÌ�culas del mismo paiÌ�s deben ordenarse por geÌ�nero. Para peliÌ�culas del mismo geÌ�nero deben ordenarse por rating IMBD. 
		//Tenga en cuenta que una peliÌ�cula puede tener asociado muÌ�ltiples paiÌ�ses y muÌ�ltiples geÌ�neros.

		//RECUERDE: los criterios pueden ser dados en cualquier orden para el filtro. 
		escritor.write("Ingrese criterio1: \n");
		escritor.flush();
		String criterio1 = lector.next();
		System.out.println(criterio1);

		escritor.write("Ingrese criterio2: \n");
		escritor.flush();
		String criterio2 = lector.next();
		System.out.println(criterio2);

		escritor.write("Ingrese el criterio3: \n");
		escritor.flush();
		String criterio3 = lector.next();
		System.out.println(criterio3);

		//RECUERDE: Utilice las variables previamente declaradas y adaptelas al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo consultarPeliculasFiltros (Integer anho, String pais, VOGeneroPelicula genero) del API 
		//Se espera como resultado: lista VOPelicula		
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}







}
